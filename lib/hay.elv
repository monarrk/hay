use str
use math

fn put_styled [s c]{
        put (styled $s $c)
}

# Center text in the terminal
fn center [line]{
	columns = (tput cols)
	printf "%*s\n" (math:round-to-even (/ (+ (count (to-string $line)) $columns) 2)) $line
}

# well formatted clock
fn clock {
	put (date "+%a %b %d, %r")
}

fn screenshot {
	scrot '%Y-%m-%d-'(date "+%T")'_scrot.png' -e 'mv $f '$E:HOME'/Pictures/shots/'
}

fn killall [a]{
  sudo kill -9 (pgrep $a)
}

use platform
use netbsd
use linux

#
# Operating system specific functions
#

# set system volume
fn set_vol [a]{
	os = (echo $platform:os)

	if (==s (put $os) "netbsd") {
		netbsd:set_vol $a
	} else {
		fail "This platform is not supported!"
	}
}

# decrease system volume
fn dec_vol [a]{
	os = (echo $platform:os)

	if (==s (put $os) "netbsd") {
		netbsd:dec_vol $a
	} else {
		fail "This platform is not supported!"
	}
}

# increase system volume
fn inc_vol [a]{
	os = (echo $platform:os)

	if (==s (put $os) "netbsd") {
		netbsd:inc_vol $a
	} else {
		fail "This platform is not supported!"
	}
}

fn get_vol {
  os = (echo $platform:os)

  if (==s (put $os) "netbsd") {
    netbsd:get_vol
  } elif (==s (put $os) "linux") {
    linux:get_vol
  } else {
    fail "This platform is not supported!"
  }
}

fn mute {
  os = (echo $platform:os)

  if (==s (put $os) "netbsd") {
    netbsd:mute
  } else {
    fail "This platform is not supported!"
  }
}

fn unmute {
  os = (echo $platform:os)

  if (==s (put $os) "netbsd") {
    netbsd:unmute
  } else {
    fail "This platform is not supported!"
  }
}

# Get the battery percentage
fn battery {
	os = (echo $platform:os)

	if (==s (put $os) "netbsd") {
		try {
			put (getbat)
		} except e {
			put (envstat -s "acpibat0:charge")
		}
	} elif (==s (put $os) "linux") {
		put (cat /sys/class/power_supply/BAT0/capacity)'%'
	} else {
		fail "This platform is not supported!"
	}
}

fn pkg_count {
	os = (echo $platform:os)

	if (==s (put $os) "netbsd") {
		netbsd:pkg_count
	} elif (==s (put $os) "linux") {
		linux:pkg_count
	} else {
		fail "This platform is not supported!"
	}
}

# Display system information
fn osinfo [@c]{
	color = "magenta"
	if (> (count $c) 0) {
		color = $c[0]
	}

	echo "\n"

        line = (styled '[ ' $color)$E:USER(styled ' at ' $color)(hostname)(styled ' ]' $color)
        center $line

        echo (put_styled "\n\t\tOPERATING SYSTEM INFORMATION" $color)

        echo "\t\t"(styled 'OS' $color)"\t: "(uname -s -m -r)
        echo "\t\t"(styled 'PKGS' $color)"\t: "(pkg_count)
	echo "\t\t"(styled 'SH' $color)"\t: "(basename $E:SHELL)
	echo "\t\t"(styled 'TERM' $color)"\t: "$E:TERM

	echo "\n"
}
