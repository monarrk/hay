# hay
A utility library for [elvish](https://elv.sh).

## Usage
Copy the files from `lib/` into `~/.elvish/lib` and add `use hay` to your `rc.elv`.

## (likely incomplete) list of functions
### Pure elvish
- `put_styled`: shorthand for putting styled strings
- `center`: center text in the terminal
- `clock`: a well formatted clock
- `screenshot`: simple screenshot command that also does some organizing for you

### OS specific functions
- `set_vol`: set system volume
- `dec_vol`: decrease system volume
- `inc_vol`: increase system volume
- `battery`: get battery percentage
- `pkg_count`: get the number of packages installed on your OS
- `osinfo`: like neofetch but worse

## Hacking
If you want to add any support for other operating systems in the OS-specific functions, you should make a file `lib/{platform}.elv` which contains the "low level" version of that function which contains the OS-specific behavior. Then, add a chain to the if statements which call those functions, as opposed to writing the OS-specific function directly to `hay.elv`. You can find an example of this in `lib/netbsd.elv`.
